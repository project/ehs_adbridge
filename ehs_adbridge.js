var EHSAdBridge = {
  queueAd : function() {
    this.ehs_tt++;
    this.ehs_tile++;
    this.ehs_x = 'tt=' + this.ehs_tt + ';' + 'tile=' + this.ehs_tile + ';';
    if (this.ehs_tile == 1) this.ehs_x = 'dcopt=ist;' + this.ehs_x;

    var output = this.printAd('t', 'l', this.ehs_ad_dimensions, this.ehs_x);

    return output;
  },
  printAd : function(ehs_vp, ehs_hp, ehs_sz, f_ehs_x) {
    if (ehs_vp != 't' && ehs_vp != 'b') ehs_vp='t';
    if (ehs_hp != 'l' && ehs_hp != 'r') ehs_hp='r';

    var path = this.ehs_protocol + 'ad.doubleclick.net/adj/' + this.ehs_target + 'vp=' + ehs_vp + ';hp=' + ehs_hp + ';' + f_ehs_x + 'sz=' + ehs_sz + ';ord=' + this.ehs_ord + '?';

    return '<script type="text/javascript" src="' + path + '"></script>';
  },
  init : function(domain, pairs, dimensions) {

    this.ehs_ad_dimensions = dimensions;

    // Zone. Maximum Chars: 31.
    // Chars allowed: Alphanumeric, -, _
    // Cannot start with a number
    this.ehs_zone = '';
    // Key-Values. Maximum Chars: 55. Chars Allowed: Alphanumeric, -, _
    // All key-values will be pushed into array and appended into
    // DART target link
    this.ehs_kv = new Array();

    for (var i=0; i < pairs.length; i++) {
      this.ehs_kv.push(pairs[i]);
    }

    this.ehs_site = domain;
    this.ehs_sitea = this.ehs_site.split('.');
    this.ehs_class = 'class=' + this.ehs_sitea[1] + ';';
    this.ehs_pub = 'pub=' + this.ehs_sitea[2] + ';';
      this.ehs_kv.push(this.ehs_class);
      this.ehs_kv.push(this.ehs_pub);
    this.ehs_sn = 'sn=' + this.ehs_sitea[3] + ';';
      this.ehs_kv.push(this.ehs_sn);
    this.ehs_zn = 'zn=' + this.ehs_zone + ';';
      this.ehs_kv.push(this.ehs_zn);
    this.ehs_path = document.location.pathname;
    this.ehs_patha = this.ehs_path.split('/');
    this.ehs_len = this.ehs_patha.length;
    for (var ehs_i=0;ehs_i<this.ehs_len;ehs_i++) {
      if (this.ehs_patha[ehs_i] != '') {
        ehs_pgp = 'pgp=' + this.ehs_patha[ehs_i].replace(/[^A-z0-9_-]/g, '_') + ';';
        this.ehs_kv.push(ehs_pgp);
      }
    }
    this.ehs_query = document.location.search;
    this.ehs_pgq = this.ehs_query.replace(/[^A-z0-9_-]/g, '_');
    this.ehs_pgq = (this.ehs_pgq.length <= 55) ? 'pgq=' + this.ehs_pgq.substring(1) + ';' : 'pgq=' + this.ehs_pgq.substring(1,55) + ';';
    this.ehs_kv.push(this.ehs_pgq);
    this.ehs_protocol = (document.location.protocol == 'https:') ? 'https://' : 'http://';
    if (this.ehs_protocol == 'https://') this.ehs_kv.push('ssl=1;');
    this.ehs_ord = Math.random()*10000000000000000;
    this.ehs_tile = 0;
    this.ehs_tt = 0;
    this.ehs_x = '';
    this.ehs_zonepat = /^[A-z]{1}[A-z0-9_-]{1,30}$/i;
    this.ehs_kvpat = /^[A-z]{1}[A-z0-9_-]{1,11}=[A-z0-9_-]{1,55};$/i;
    this.ehs_target = this.ehs_site;
    this.ehs_target += (this.ehs_zonepat.test(this.ehs_zone)) ? '/' + this.ehs_zone + ';' : ';';
    this.ehs_len = this.ehs_kv.length;
    for (ehs_i=0;ehs_i<this.ehs_len;ehs_i++) {
      this.ehs_target += (this.ehs_kvpat.test(this.ehs_kv[ehs_i])) ? this.ehs_kv[ehs_i] : '';
    }

    return this;
  }
};

