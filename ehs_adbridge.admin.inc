<?php
/**
 * @file
 * Functions for block administration and viewing.
 */

// Hook implementations

/**
 * Settings form.
 */
function ehs_adbridge_form_settings() {
  $config = _ehs_adbridge_variable_get(NULL);

  $intro = t('After the settings have been applied, proceed to the <a href="@overview">block overview page</a> to start creating ad blocks.', array('@overview' => url('admin/build/block')));;

  $form = array(
    '#prefix' => $intro
  );

  $form['ehs_adbridge_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Ad Server Domain'),
    '#description' => t('Set the domain for your EHS ad server instance (e.g. "@ehs_adbridge_domain").', array('@ehs_adbridge_domain' => $config['ehs_adbridge_domain'])),
    '#required' => TRUE,
    '#default_value' => _ehs_adbridge_variable_get('ehs_adbridge_domain'),
  );

  $form['ehs_adbridge_dimensions'] = array(
    '#type' => 'textarea',
    '#title' => t('Available ad dimensions'),
    '#description' => t('Enter WIDTHxHEIGHT pairs delimited with a space. Example:') . ' 728x90 160x600 468x60 300x250',
    '#required' => TRUE,
    '#default_value' => _ehs_adbridge_variable_get('ehs_adbridge_dimensions'),
  );

  $form = system_settings_form($form);

  // Flush all caches to force our hooks to be run again
  // (particularly hook_menu_alter)
  $form['#submit'][] = 'drupal_flush_all_caches';

  return $form;
}
/**
 * Implements hook_form_alter().
 */
function ehs_adbridge_form_alter(&$form, &$form_state, $form_id) {
  switch ($form['#id']) {
    case 'block-admin-display-form':

      $blocks_settings = _ehs_adbridge_variable_get('ehs_adbridge_blocks');
      $deltas_original = array_keys($blocks_settings);

      foreach ($blocks_settings as $delta => $values) {
        $form['ehs_adbridge_' . $delta]['delete'] = array('#value' => filter_xss(l(t('delete'), 'admin/build/block/ehs_bridge/delete/' . $delta)));
      }

      // Get multiblock instances; add/update links
      if (module_exists('multiblock')) {
        if (!empty($deltas_original)) {
          $placeholders = implode(",", array_fill(0, count($deltas_original), "%d"));
          $result_multi = db_query("SELECT m.delta,m.orig_delta FROM {multiblock} AS m WHERE m.module = 'ehs_adbridge' AND m.orig_delta IN ($placeholders) AND m.multi_settings = 1", $deltas_original);
          while ($row = db_fetch_array($result_multi)) {
            // Add delete link to ehs_adbridge instances
            $form['multiblock_' . $row['delta']]['delete'] = array('#value' => filter_xss(l(t('delete'), 'admin/build/block/instances/delete/' . $row['delta'])));

            // Update configure link to point to ehs_adbridge parent
            $form['multiblock_' . $row['delta']]['configure'] = array('#value' => filter_xss(l(t('configure'), 'admin/build/block/configure/ehs_adbridge/' . $row['orig_delta'])));

            // Remove configure link
            // unset($form['multiblock_' . $row['delta']]['configure']);
          }
        }
      }

      break;
    case 'multiblock-add-form':

      if (module_exists('multiblock')) {
        // Append submit callback to multiblock admin form
        $form['#submit'][] = 'ehs_adbridge_form_block_configure_submit_multiblock';
      }

      break;
  }
}

// Block operations

/**
 * Returns the 'list' $op info for hook_block().
 */
function ehs_adbridge_block_list() {
  $blocks = array();
  $blocks_settings = _ehs_adbridge_variable_get('ehs_adbridge_blocks');

  foreach ($blocks_settings as $delta => $values) {
    $blocks[$delta]['info'] = check_plain('EHS Ad - ' . $values['title']);
    $blocks[$delta]['cache'] = BLOCK_NO_CACHE;
  }

  return $blocks;
}
/**
 * Returns the 'configure' $op info for hook_block().
 */
function ehs_adbridge_block_configure($delta, $edit) {

  if (!user_access('use PHP for block visibility')) {
    drupal_set_message('You do not have permission to use PHP for block visibility. Please contact your site administrator for resolution.');
    drupal_goto('admin/build/block');
    return '';
  }

  return ehs_adbridge_form_block_configure($delta, $edit);
}
/**
 * Returns the 'mb_enabled' $op info for hook_block().
 */
function ehs_adbridge_block_mb_enabled($delta, $edit) {
  return 'mb_enabled';
}
/**
 * Returns the 'save' $op info for hook_block().
 */
function ehs_adbridge_block_save($delta, $edit) {
  return ehs_adbridge_form_block_configure_submit($delta, $edit);
}
/**
 * Shared 'configure' $op info for save and add ops.
 */
function ehs_adbridge_form_block_configure($delta, $edit) {

  $blocks = _ehs_adbridge_variable_get('ehs_adbridge_blocks');
  $dimensions = _ehs_adbridge_variable_get('ehs_adbridge_dimensions');
  $dimensions = explode(' ', trim(check_plain($dimensions)));
  sort($dimensions);

  // Create associative array for dimension options
  $dimensions = array_flip($dimensions);
  foreach ($dimensions as $k => $v) {
    $dimensions[$k] = $k;
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('EHS Ad title'),
    '#maxlength' => 64,
    '#description' => t('The block title to be used on the <a href="@overview">block overview page</a>. Titles will be automatically prefixed with "EHS Ad -".', array('@overview' => url('admin/build/block'))),
    '#weight' => -18,
    '#required' => TRUE,
  );

  $form['dimensions'] = array(
    '#type' => 'select',
    '#title' => t('EHS Ad dimension'),
    '#options' => $dimensions,
    '#required' => TRUE,
  );

  $form['pairs'] = array(
    '#type' => 'textarea',
    '#title' => t('EHS key/value pairs'),
    '#description' => t('Example:') . ' mcat1=me22; mcat2=null; mcat3=null; mcon1=d35n; mcon2=s109s; mcon3=null; cpath=null; rcat=null; rcon=null; rpath=null;',
    '#required' => TRUE,
  );


  if (!empty($delta)) {
    $form['pairs']['#default_value'] = $blocks[$delta]['pairs'];
    $form['dimensions']['#default_value'] = $blocks[$delta]['dimensions'];
    $form['title']['#default_value'] = $blocks[$delta]['title'];
  }

  return $form;
}
/**
 * Save the updated block.
 */
function ehs_adbridge_form_block_configure_submit($delta, $edit) {
  if (!form_get_errors()) {
    $config = _ehs_adbridge_variable_get('ehs_adbridge_blocks');

    $config[$delta] = array(
      'title' => $edit['title'],
      'dimensions' => $edit['dimensions'],
      'pairs' => trim(str_replace(";", "; ", str_replace(array("\n", "\r", " "), "", $edit['pairs']))),
    );

    variable_set('ehs_adbridge_blocks', $config);


    $pages = trim($edit['pages']);
    $php = $pages;
    $check = strstr($pages, '<?php');

    if (empty($check)) {
      $php = '<?php
return _ehs_adbridge_block_is_visible("' . $pages . '");
?>';
    }

    if (!empty($pages)) {
      db_query("UPDATE {blocks} SET visibility = 2, pages = '%s' WHERE module = '%s' AND delta = '%s'", array(
        $php,
        $edit['module'],
        $delta,
      ));
    }

    // Update any multiblock instances with new visibility settings
    if (module_exists('multiblock')) {
      db_query("UPDATE blocks AS b1, blocks AS b2 SET
          b1.visibility = b2.visibility,
          b1.pages = b2.pages
          WHERE b1.module = 'multiblock'
          AND b1.delta IN (SELECT m.delta FROM multiblock AS m WHERE m.module = 'ehs_adbridge' AND m.orig_delta = '%s')
          AND b2.module = 'ehs_adbridge'
          AND b2.delta = '%s'", array(
        $delta,
        $delta,
      ));

      drupal_set_message(t('Instances updated with parent EHS Ad block page visibility settings.'));
    }

    // Flush all caches
    drupal_flush_all_caches();

    $form_state['redirect'] = 'admin/build/block';
    return;
  }
}
/**
 * Menu callback: display the block addition form.
 */
function ehs_adbridge_form_block_add(&$form_state) {
  module_load_include('inc', 'block', 'block.admin');

  $form = block_admin_configure($form_state, 'ehs_adbridge', NULL);

  return $form;
}
/**
 * Save the new block.
 */
function ehs_adbridge_form_block_add_submit($form, &$form_state) {

  // Determine the delta of the new block.
  $config = _ehs_adbridge_variable_get('ehs_adbridge_blocks');
  $delta = empty($config) ? 1 : count($config) + 1;

  // Save the new block config
  $config[$delta] = array(
    'title' => $form_state['values']['title'],
    'dimensions' => $form_state['values']['dimensions'],
    'pairs' => $form_state['values']['pairs'],
  );

  variable_set('ehs_adbridge_blocks', $config);

  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      db_query("INSERT INTO {blocks} (visibility, pages, custom, title, module, theme, status, weight, delta, cache) VALUES(%d, '%s', %d, '%s', '%s', '%s', %d, %d, '%s', %d)", $form_state['values']['visibility'], trim($form_state['values']['pages']), $form_state['values']['custom'], $form_state['values']['title'], $form_state['values']['module'], $theme->name, 0, 0, $delta, BLOCK_NO_CACHE);
    }
  }

  foreach (array_filter($form_state['values']['roles']) as $rid) {
    db_query("INSERT INTO {blocks_roles} (rid, module, delta) VALUES (%d, '%s', '%s')", $rid, $form_state['values']['module'], $delta);
  }

  // Update new block with PHP visibility stuff
  ehs_adbridge_form_block_configure_submit($delta, $form_state['values']);

  drupal_set_message(t('The "@name" block has been added.', array('@name' => $form_state['values']['title'])));
  cache_clear_all();

  $form_state['redirect'] = 'admin/build/block';
  return;
}

/**
 * MultiBlock save (currently unused)
 *
 * See ehs_adbridge_form_alter case 'multiblock-add-form' for the implementation
 */
function ehs_adbridge_form_block_configure_submit_multiblock($form, &$form_state) {

  $delta = NULL;
  $instance = NULL;

  if (array_key_exists('block', $form_state['values'])) {
    preg_match('/ehs_adbridge?(.*?)\\1/', $form_state['values']['block'], $matches);
    foreach ($matches as $k => $v) {
      if ($v == 'ehs_adbridge') {
        // New ehs_adbridge multiblock instance
        $delta = db_last_insert_id('multiblock', 'delta');
      }
    }
  }
  elseif (array_key_exists('instance', $form_state['values'])) {
    // New multiblock instance
    $delta = $form_state['values']['instance'];
  }

  // We have a multiblock delta; make sure we're
  // updating the correct ehs_adbridge instance
  if (!empty($delta)) {
    $instances = multiblock_get_block(NULL, TRUE);
    foreach ($instances as $d => &$i) {
      if ($i->module == 'ehs_adbridge' && $i->delta == $delta) {
        $instance = $i;
      }
    }
  }

  // Update the multiblock block record with orig_delta's visibility
  // settings This saves extra queries per instance during the hook_block
  // 'view' operation
  if (!empty($instance)) {
    db_query("UPDATE blocks AS b1, blocks AS b2 SET
        b1.visibility = b2.visibility,
        b1.pages = b2.pages
        WHERE b1.module = 'multiblock'
        AND b1.delta = '%s'
        AND b2.module = 'ehs_adbridge'
        AND b2.delta = '%s'", array(
      $instance->delta,
      $instance->orig_delta,
    ));

    drupal_set_message(t('Instance updated with parent EHS Ad block page visibility settings.'));
  }

}

// Block delete

/**
 * Menu callback: confirm deletion of blocks.
 */
function ehs_adbridge_form_block_delete(&$form_state, $delta = 0) {
  $config = _ehs_adbridge_variable_get('ehs_adbridge_blocks');
  $title = check_plain($config[$delta]['title']);
  $form['block_title'] = array('#type' => 'hidden', '#value' => $title);
  $form['delta'] = array('#type' => 'hidden', '#value' => $delta);

  $message = 'Are you sure you want to delete the "@name" block?';

  // Also confirm deletion of multiblock instances
  if (module_exists('multiblock')) {
    $multiblocks = multiblock_get_block(NULL, TRUE);
    $multiblocks_ix = 0;
    $message_multi .= 'Note: this will also delete the following MultiBlock instances of the block: <ul>';
    foreach ($multiblocks as $block) {
      if ($block->module == 'ehs_adbridge' && $block->orig_delta == $delta) {
        $title = check_plain($block->title);
        $message_multi .= '<li>' . $title . '</li>';
        $multiblocks_ix++;
      }
    }
    $message_multi .= '</ul>';

    if (empty($multiblocks_ix)) {
      $form['#prefix'] = t('Note: there are no MultiBlock instances associated with this block.');
    }
    else {
      $form['#prefix'] = t($message_multi);
    }
  }

  return confirm_form($form, t($message, array('@name' => $title)), 'admin/build/block', NULL, t('Delete'), t('Cancel'));
}
/**
 * Delete a block.
 */
function ehs_adbridge_form_block_delete_submit($form, &$form_state) {

  $delta = $form_state['values']['delta'];

  // Delete all multiblock instances of this block
  if (module_exists('multiblock')) {
    $multiblocks = multiblock_get_block(NULL, TRUE);
    foreach ($multiblocks as $block) {
      if ($block->module == 'ehs_adbridge' && $block->orig_delta == $delta) {
        $form_state_multi['values'] = (array) $block;
        multiblock_delete_form_submit($form, $form_state_multi);
      }
    }
  }

  $config = _ehs_adbridge_variable_get('ehs_adbridge_blocks');

  // Remove the block configuration variables.
  unset($config[$delta]);

  ksort($config, SORT_NUMERIC);
  variable_set('ehs_adbridge_blocks', $config);

  db_query("DELETE FROM {blocks} WHERE module = 'ehs_adbridge' AND delta = %d", $delta);
  db_query("DELETE FROM {blocks_roles} WHERE module = 'ehs_adbridge' AND delta = %d", $delta);

  drupal_set_message(t('The "@name" block has been removed.', array('@name' => $form_state['values']['block_title'])));
  cache_clear_all();

  $form_state['redirect'] = 'admin/build/block';
  return;
}

