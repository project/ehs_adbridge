==================================
AUTHOR
==================================
  Nate Mow
  natemow@gmail.com

==================================
DESCRIPTION
==================================

  Adds required Javascript snippets in to theme regions to allow 
  e-Healthcare Solutions (EHS) "Premium Advertising Network" ads 
  to run on your site.

  It's important to note that this module automatically sets 
  page specific visibility based on a PHP return value; there is 
  no need to set the visibility option, only the list of pages you 
  want to include the ads on. The goal with this design decision is 
  to create a central callback for each ehs_adbridge block that is 
  created in the system. Eventually, a hook may be added to enable 
  child modules to control visibility directly.

  As such, an account must have the "use PHP for block visibility" 
  permission in addition to the "administer ehs_adbridge" permission 
  in order to use the module.

  The module is also compatible with MultiBlock. In the case that 
  you create a multiblock instance of an ehs_adbridge block, the 
  instance will inherit the parent block's settings (e.g. EHS 
  key/val pairs, page visibility, etc.

